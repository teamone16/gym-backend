import { get, runRawQuery } from '../db.mjs';
import { validateReq } from '../helper.mjs';


const createMemberships = async (req, res, next) => {
    validateReq(req, res);

    try {
        const reqBody = req.body;
        get().getConnection((error, connection)=> {
            createMembArr =[[reqBody.membership_name, reqBody.membership_duration, reqBody.membership_amount, reqBody.membership_notes, 1]]
            connection.query('INSERT INTO tblmembership(mem_name, mem_duration, mem_amount,mem_notes, is_active) VALUES(?)', createMembArr, (error, results, fields)=> {
                if(error) {
                    console.log(error);
                    return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                } else {
                    res.status(200).send({"status": 200, "message": "Membership created succesfully!", "success": true })
                }
            })
        })

    } catch(error){
        console.log(error);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}

const getMemberships = async(req, res, next) => {
    let  {limit} = req.query
    if(limit  === undefined) {
        limit = 15;
    }
    try {
        let getMemData = await runRawQuery(`SELECT mem_id, mem_name, mem_duration, mem_amount FROM tblmembership WHERE is_active = 1 LIMIT ${limit}`);

        let totalCount = await runRawQuery(`select count(*) as count from tblmembership where is_active=1`);
        totalCount = JSON.parse(JSON.stringify(totalCount))[0].count;

        let resObj = {
            "status": 200,
            "success": true,
            "limit": limit,
            "totalPages": Math.ceil(parseInt(totalCount)/limit),
            "message": "Membership data has been fetched successfully",
            "data": getMemData
        }
        res.send(resObj);

    } catch(error) {
        console.log(error);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}

const deleteMembership = async(req, res, next) => {
    try {
        let queryRes = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tblmembership WHERE mem_id = '${req.params.id}' AND is_active = 1) AS checkId`);
        queryRes = JSON.parse(JSON.stringify(queryRes))[0].checkId;

        if(queryRes === 1) {
            await runRawQuery(` UPDATE tblmembership SET is_active = 0 WHERE mem_id = ${req.params.id}`);
            res.status(200).send({"status": 200, "message": "Membership deleted Succesfully!" , "success": true})
        } else {
            res.status(404).send({"status": 404 , "message": "Membership doesn't exist." , "success": false})
        }
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}


export {createMemberships, getMemberships, deleteMembership}