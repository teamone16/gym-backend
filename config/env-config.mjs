export default {
    prod : {
        env: 'dev',
        port: 8000,
        mysql: {
            host: '127.0.0.1',
            port: 3306,
            user: 'root',
            password: '12345',
            db: 'gym'
        },
        cookieKey: '123123123',
        googleClientID: 'testID',
        googleClientSecret: 'testSecret'
    },
    dev: {
        env: 'dev',
        port: 8000,
        mysql: {
            host: '127.0.0.1',
            port: 3306,
            user: 'root',
            password: '123456',
            db: 'gym'
        },
        cookieKey: '123123123',
        googleClientID: 'testID',
        googleClientSecret: 'testSecret'
    }
}