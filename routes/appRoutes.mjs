import express from 'express';
import { updateUser, getUsers, deleteUser } from '../controllers/userController.mjs'
import { createQuery, moveQuery, getQuery } from '../controllers/queryController.mjs';
import { createMemberships, getMemberships, deleteMembership } from '../controllers/membershipController.mjs'
import { createEmployee, getEmployee, deleteEmployee, updateEmployee } from '../controllers/employeeController.mjs';
import * as Eval from 'express-validator/check';
import { deleteQuery } from '../controllers/queryController.mjs';
import { getStatistics } from '../controllers/statisticsController.mjs';

const check = Eval.default.check;
const router = express.Router();

// User Routes.

router.get('/user', getUsers);

router.put('/user/:id', [
    check('f_name').trim().not().isEmpty().withMessage('First Name is Mandatory.'),

    check('email').not().isEmpty().withMessage('Email is Mandatory.')
        .isEmail().withMessage('Please Enter a Valid Email Address.'),

    check('contact').not().isEmpty().withMessage('Contact Number is Mandatory.')
        .isInt().withMessage('Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Contact Number.'),
    
    check('eme_contact_name').trim().not().isEmpty().withMessage('Emergency Contact Name is Mandatory.'),

    check('eme_contact_number').not().isEmpty().withMessage('Emergency Contact Number is Mandatory.')
        .isInt().withMessage('Emergency Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Emergency Contact Number.'),
], updateUser);

router.delete('/user/:id', deleteUser)

// Query Routes

router.get('/query', getQuery)

router.post('/query', [
    check('f_name').trim().not().isEmpty().withMessage('First Name is Mandatory.'),

    check('email').not().isEmpty().withMessage('Email is Mandatory.')
        .isEmail().withMessage('Please Enter a Valid Email Address.'),

    check('contact').not().isEmpty().withMessage('Contact Number is Mandatory.')
        .isInt().withMessage('Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Contact Number.'),

    check('query.followup_date').trim().not().isEmpty().withMessage('Followup Date is Mandatory.'),    
], createQuery)

router.put('/query/move/:user_id', [
    check('query_id').trim().not().isEmpty().withMessage('Query Id is Mandatory.')
        .isInt().withMessage('Query Id Should have only Numbers.'),

    check('mem_id').trim().not().isEmpty().withMessage('Membership Id is Mandatory.')
        .isInt().withMessage('Membership Id Should have only Numbers.'),

    check('f_name').trim().not().isEmpty().withMessage('First Name is Mandatory.'),

    check('email').not().isEmpty().withMessage('Email is Mandatory.')
        .isEmail().withMessage('Please Enter a Valid Email Address.'),
    
    check('contact').not().isEmpty().withMessage('Contact Number is Mandatory.')
        .isInt().withMessage('Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Contact Number.'),
    
    check('dob').trim().not().isEmpty().withMessage('DOB is Mandatory.'),

    check('gender').trim().not().isEmpty().withMessage('Gender is Mandatory.'),
   
    check('eme_contact_name').trim().not().isEmpty().withMessage('Emergency Contact Name is Mandatory.'),
    
    check('eme_contact_number').not().isEmpty().withMessage('Emergency Contact Number is Mandatory.')
        .isInt().withMessage('Emergency Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Emergency Contact Number.'),

    check('transaction.amount').not().isEmpty().withMessage('Amount is Mandatory')
        .isInt().withMessage('Amount Should have only Numbers.'),
    
    check('transaction.source').not().isEmpty().withMessage('Source is Mandatory'),

    check('transaction.invoice_id').not().isEmpty().withMessage('Invoice Id is Mandatory')
        .isInt().withMessage('Invoice Id Should have only Numbers.'),
], moveQuery)

router.delete('/query/:query_id', deleteQuery);

// Memebership Routes
router.get,('/memberships', getMemberships);

router.post('/memberships', [
    check('membership_name').trim().not().isEmpty().withMessage('Membership Name is Mandatory.'),
    
    check('membership_duration').not().isEmpty().withMessage('Membership Duration is Mandatory.')
        .isInt().withMessage('Membership Duration Should have only Numbers.'),

    check('membership_amount').not().isEmpty().withMessage('Membership Amount is Mandatory.')
        .isInt().withMessage('Membership Amount Should have only Numbers.'),
    
], createMemberships);

router.delete('/memberships/:id', deleteMembership);


//  Employee Routes
router.get('/employee', getEmployee);

router.post('/employee', [
    check('f_name').trim().not().isEmpty().withMessage('First Name is Mandatory.'),

    check('email').not().isEmpty().withMessage('Email is Mandatory.')
        .isEmail().withMessage('Please Enter a Valid Email Address.'),

    check('password').not().isEmpty().withMessage('Password is Mandatory.')
        .isLength({ min: 8, max: 20 }).withMessage('Please Enter a Valid Password.'),

    check('gender').trim().not().isEmpty().withMessage('Gender is Mandatory.'),

    
    check('access_level').not().isEmpty().withMessage('Access Level is Mandatory.')
        .isInt().withMessage('Access Level Should have only Numbers.')
        .isLength({ min: 1, max: 1 }).withMessage('Please Enter a Valid Access Level.'),

    check('contact').not().isEmpty().withMessage('Contact Number is Mandatory.')
        .isInt().withMessage('Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Contact Number.'),

    check('eme_contact_name').trim().not().isEmpty().withMessage('Emergency Contact Name is Mandatory.'),

    check('eme_contact_number').not().isEmpty().withMessage('Emergency Contact Number is Mandatory.')
        .isInt().withMessage('Emergency Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Emergency Contact Number.'),

], createEmployee)


router.delete('/employee/:id', deleteEmployee);

router.put('/employee/:id', [
    check('f_name').trim().not().isEmpty().withMessage('First Name is Mandatory.'),

    check('contact').not().isEmpty().withMessage('Contact Number is Mandatory.')
    .isInt().withMessage('Contact Number Should have only Numbers.')
    .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Contact Number.'),

    check('gender').trim().not().isEmpty().withMessage('Gender is Mandatory.'),

    check('eme_contact_name').trim().not().isEmpty().withMessage('Emergency Contact Name is Mandatory.'),

    check('eme_contact_number').not().isEmpty().withMessage('Emergency Contact Number is Mandatory.')
        .isInt().withMessage('Emergency Contact Number Should have only Numbers.')
        .isLength({ min: 10, max: 10 }).withMessage('Please Enter a Valid Emergency Contact Number.'),

], updateEmployee)

router.get('/stats', getStatistics);

export {router as appRoutes };
