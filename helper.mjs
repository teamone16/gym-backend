import * as Eval from 'express-validator/check';

const validationResult = Eval.default.validationResult

// Helper Function to Add 0 for date < 10
const addZero = (val)=> {
    if(val < 10) return `0${val}`;
    else return val;  
} 

// Converting A Date to YYYY-MM-DD HH:MM:SS
const DateToDBFormat = (d)=> {
    d = new Date(d);
    let fomatedDate = [addZero(d.getFullYear()), addZero(d.getMonth()+1), addZero(d.getDate())].join('-')
        +' '+
       [addZero(d.getHours()), addZero(d.getMinutes()), addZero(d.getSeconds())].join(':');
    return fomatedDate
}

const DateDDMMYY = (d)=> {
    d = new Date(d);
    let newDate = [addZero(d.getDate()), addZero(d.getMonth()+1), d.getFullYear()].join('-');
    return newDate;
}

// Adding X Months to Given Date.
const calculateFutureDate = (date, months) => {
    date = new Date(date);
    date.setMonth(date.getMonth()+ parseInt(months));
    return DateToDBFormat(date);
} 

// Removing X Months to Given Date.
const calculatePastDate = (date, months) => {
    date = new Date(date);
    date.setMonth(date.getMonth() - parseInt(months));
    return DateToDBFormat(date);
}

// Handler to validate Request Data.
const validateReq = (req,res)=> {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }
}


export { calculateFutureDate, calculatePastDate, DateToDBFormat, validateReq, DateDDMMYY }
