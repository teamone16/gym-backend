// "memberships": 10,
// "querys": 5,
// "employees": 10,
// "customers": 20,
// "income": 1000,
// "expenditure": 100
import { runRawQuery } from '../db.mjs';
import { calculatePastDate } from '../helper.mjs';

const getStatistics = async (req, res, next) => {
    try {
        let  {months} = req.query
        if(months  === undefined) {
            months = 1;
        }
        const pastDate = calculatePastDate(new Date(), months);
        let memCount = await runRawQuery(`SELECT COUNT(*) AS MEM_TOT from tblmembership WHERE is_active = 1;`);
        memCount = JSON.parse(JSON.stringify(memCount))[0].MEM_TOT;

        let empCount = await runRawQuery(`SELECT COUNT(*) AS EMP_TOT from tblemployees WHERE is_active = 1;`);
        empCount = JSON.parse(JSON.stringify(empCount))[0].EMP_TOT;

        let custCount = await runRawQuery(`SELECT COUNT(*) AS USER_TOT from tbluser WHERE fk_query_id = 0 AND created_at > '${pastDate}';`)
        custCount = JSON.parse(JSON.stringify(custCount))[0].USER_TOT;

        let queryCount = await runRawQuery(`SELECT COUNT(*) AS QUERY_TOT from tbluser WHERE fk_query_id != 0 AND created_at > '${pastDate}';`)
        queryCount = JSON.parse(JSON.stringify(queryCount))[0].QUERY_TOT;

        let income = await runRawQuery(`SELECT sum(amount) AS INCOME from tbltransactions where status = 'CREDIT' and created_at > '${pastDate}'`);
        income = JSON.parse(JSON.stringify(income))[0].INCOME;

        let exp = await runRawQuery(`SELECT sum(amount) AS EXPENDITURE from tbltransactions where status = 'DEBIT' and created_at > '${pastDate}'`);
        exp = JSON.parse(JSON.stringify(exp))[0].EXPENDITURE;


        
        const resObj = {
            "memberships": memCount,
            "querys": queryCount,
            "employees": empCount,
            "customers": custCount,
            "income": income,
            "expenditure": exp
        }
        return res.status(200).send({"status": 200, "message": "Data retrived succesfully", "data": resObj, "success": true});

    } catch(err) {
        console.log(err);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}

export { getStatistics }