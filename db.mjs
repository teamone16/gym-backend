import mysql from 'mysql';
import config from './config/env';

let state = {
    pool: null,
    env: null
};

const connect = (env, done)=> {
    state.pool = mysql.createPool({
        host: config.mysql.host,
        port: config.mysql.port,
        user: config.mysql.user,
        password: config.mysql.password,
        database: config.mysql.db 
    })
    
    state.env = env;
    done()
}

const get = () => {
    return state.pool;
}

const runRawQuery = (query) => {
    return new Promise ((resolve, reject) => {
        get().query(query, (err, rows) => {
            if (err) { reject(err);}
            resolve(rows)
        });
    });
}

const getAllData = (tableName)=> {
    let createQuery = `select * from ${tableName}`;
    try{
        return runRawQuery(createQuery);
    } catch(e){
        console.log(e);
    }
}

export { connect, get , runRawQuery, getAllData}
