import { get, runRawQuery } from '../db.mjs';
import { DateToDBFormat, DateDDMMYY, validateReq, calculateFutureDate } from '../helper.mjs';


const createQuery = async (req, res, next) => {
    validateReq(req,res)

    try {
        let queryRes = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tbluser WHERE contact = '${req.body.contact} AND fk_query_id != 0') AS checkCount`);
        queryRes = JSON.parse(JSON.stringify(queryRes))[0].checkCount;

        if(queryRes === 0) {
            const reqObj = req.body;
            get().getConnection((err, connection) => {
                connection.beginTransaction((err)=> {
                    if (err) { throw err; }
                    const queryArr = [[reqObj.query.followup_date, reqObj.query.followup_notes,'1']];
                    connection.query(`INSERT INTO tblquery(followup_date,followup_notes,is_active) VALUES(?)`, queryArr, (error, results, fields) => {

                        if(error) {
                            return connection.rollback(() => {
                                console.log(error);
                                return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                            });
                        }

                        const userArr = [[reqObj.f_name, reqObj.l_name, reqObj.email, reqObj.contact, results.insertId, 0]]

                        connection.query(`INSERT INTO tbluser(f_name,l_name,email,contact,fk_query_id, is_active) VALUES(?)`, userArr,(error, results, fields) => {

                            if(error) {
                                return connection.rollback(() => {
                                    console.log(error);
                                    return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                });
                            }
                            
                            connection.commit((err) => {
                                if (err) {
                                    return connection.rollback(() => {
                                        console.log(error);
                                        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                    });
                                }
                                return res.status(200).send({"status": 200, "message": "Query has been created Succesfully", "success": true});            
                            });
                        })
                    })
                })
            })

        } else {
            return res.status(422).send({"status": 422, "message": "Query/User already exist" , "success": false});
        }
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "succcess": false});
    }
}

const getQuery = async(req, res, next) => {
    try{
        let  {limit} = req.query
        if(limit  === undefined) {
            limit = 15;
        }
        let data = await runRawQuery(`SELECT query_id, followup_date, followup_notes FROM tblquery WHERE is_active=1 LIMIT ${limit}`);

        for( let obj of data) {
            let userData = await runRawQuery(`SELECT user_id, f_name, l_name, email, contact FROM tbluser WHERE fk_query_id = ${obj.query_id}`); 
            obj.user = userData  
            obj.followup_date = DateDDMMYY(obj.followup_date)
        }
        let totalCount = await runRawQuery(`select count(*) as count from tblquery where is_active=1`);
        totalCount = JSON.parse(JSON.stringify(totalCount))[0].count;

        let resObj = {
            "status": 200,
            "success": true,
            "limit": limit,
            "totalPages": Math.ceil(parseInt(totalCount)/limit),
            "message": "Query data has been fetched successfully",
            "data": data

        }
        res.send(resObj);
       
    } catch(err) {
        console.log(err);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
};

const moveQuery = async (req, res, next) => {
    try {
        validateReq(req,res)
        let reqObjMove = req.body;
        get().getConnection((err, connection) => {
            connection.beginTransaction((err) => {
                if(err) {throw err; }
                let transactionArr = [[reqObjMove.transaction.invoice_id, reqObjMove.transaction.amount, 'CREDIT',  reqObjMove.transaction.source, reqObjMove.transaction.reason]];

                connection.query(`INSERT INTO tbltransactions(invoice_number, amount, status, source, reason) VALUES (?)`, transactionArr, (error, results, fields) => {
                    if(error) {
                        return connection.rollback(() => {
                            console.log(error);
                            return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                        });
                    }

                    const transactionId = results.insertId;
                    
                    connection.query(`UPDATE tblquery SET is_active = 0 WHERE query_id = ${reqObjMove.query_id}`, (error, results) => {
                        if(error) {
                            return connection.rollback(()=>{
                                console.log(error);
                                return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                            });
                        }

                        const updateUser = [reqObjMove.f_name, reqObjMove.l_name,reqObjMove.contact,reqObjMove.email,DateToDBFormat(reqObjMove.dob),reqObjMove.gender,reqObjMove.eme_contact_name, reqObjMove.eme_contact_number, 0, reqObjMove.query_id]
                        connection.query('UPDATE tbluser SET f_name= ?, l_name= ?, contact = ?, email=?, dob=?, gender=?, eme_contact_name=?,eme_contact_number=? , fk_query_id=? WHERE fk_query_id = ?',updateUser,  (error,results)=>{

                            if(error) {
                                return connection.rollback(()=>{
                                    console.log(error);
                                    return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                });
                            }

                            connection.query(`SELECT mem_duration from tblmembership WHERE mem_id=${reqObjMove.mem_id}`, async (error, results)=> {
                                if(error) {
                                    return connection.rollback(()=>{
                                        console.log(error);
                                        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                    });
                                }
                                const duration = JSON.parse(JSON.stringify(results))[0].mem_duration;
                                const startDate = await DateToDBFormat(reqObjMove.mem_start_date);
                                const endDate = await calculateFutureDate(startDate, duration);
                                const historyArr = [[req.params.user_id, reqObjMove.mem_id, transactionId, startDate, endDate, 1 ]];
                                connection.query(`UPDATE tblhistorymemb SET is_active = 0 WHERE fk_user_id =${req.params.user_id}`, (error, results)=> {
                                    if(error) {
                                        return connection.rollback(()=>{
                                            console.log(error);
                                            return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                        });
                                    }
                                    connection.query('INSERT INTO tblhistorymemb (fk_user_id, fk_mem_id, fk_trans_id, mem_start, mem_end, is_active) VALUES(?)',historyArr, (error, results)=> {
                                        if(error) {
                                            return connection.rollback(()=>{
                                                console.log(error);
                                                return res.status(500).send({"status": 500, "message": "Internal Server Error","success": false});
                                            });
                                        }
                                        connection.commit((err) => {
                                            if (err) {
                                                return connection.rollback(() => {
                                                    console.log(error);
                                                    return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                                });
                                            }
                                            return res.status(200).send({"status": 200, "message": "Membership created succesfully.", "success": true});            
                                        });
                                    })
                                })
                            })
                        })
                    })
                    
                })

            })
        })
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error"});
    }
}

const deleteQuery = async(req, res, next) => {
    try {
        let delQuery = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tblquery WHERE query_id = '${req.params.query_id}' AND is_active = 1) AS checkId`);
        delQuery = JSON.parse(JSON.stringify(delQuery))[0].checkId;
        
        if(delQuery === 1) {

            get().getConnection((err, connection) => {
                connection.query(`UPDATE tblquery SET is_active = 0 WHERE query_id = ${req.params.query_id}`, (error, fields)=> {
                    if(error) {
                        return connection.rollback(()=>{
                            console.log(error);
                            return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                        }); 
                    } 
                    connection.query(`DELETE FROM tbluser WHERE fk_query_id = '${req.params.query_id}'`, (error, fields)=> {
                        if(error) {
                            return connection.rollback(()=>{
                                console.log(error);
                                return res.status(500).send({"status": 500, "message": "Internal Server Error","success": false});
                            });
                        }
                        connection.commit((err) => {
                            if (err) {
                                return connection.rollback(() => {
                                    console.log(error);
                                    return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                                });
                            }
                            res.status(200).send({"status": 200, "message": "Query deleted Succesfully!", "success": true})            
                        });
                    })
                })
            })
          
        } else {
            res.status(404).send({"status": 404 , "message": "User doesn't exist.", "success": false})
        }
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
};

const updateQuery = (req, res, next) => {

}

export { createQuery, moveQuery, getQuery, deleteQuery, updateQuery }