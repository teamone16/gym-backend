import { get, runRawQuery } from '../db.mjs';
import { validateReq, DateToDBFormat, DateDDMMYY } from '../helper.mjs';



const getUsers = async (req, res, next)=> {
    let  {limit} = req.query
    if(limit  === undefined) {
        limit = 15;
    }
    try {
        let data = await runRawQuery(`SELECT user_id, f_name, l_name, email, contact, dob, gender, eme_contact_name, eme_contact_number,created_at FROM tbluser WHERE is_active=1 LIMIT ${limit}`);
        let totalCount = await runRawQuery(`select count(*) as count from tbluser where is_active=1`);
        totalCount = JSON.parse(JSON.stringify(totalCount))[0].count;
       
        data.forEach((obj) => {
            obj.dob = DateDDMMYY(obj.dob)
            obj.created_at = DateDDMMYY(obj.created_at);
        });
    
        let resObj = {
            "status": 200,
            "success": true,
            "limit": limit,
            "totalPages": Math.ceil(parseInt(totalCount)/limit),
            "message": "User data has been fetched successfully",
            "data": data

        }
        res.send(resObj);
    } catch(e){
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }

}


const updateUser = async(req, res, next)=> {
    validateReq(req,res);

    try {
        let queryRes = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tbluser WHERE user_id = '${req.params.id}') AS checkId`);
        queryRes = JSON.parse(JSON.stringify(queryRes))[0].checkId;
        
        if(queryRes === 1) {
            const reqObjUser = req.body;
           
            get().getConnection((err, connection) => {
                const updateUserArr = [reqObjUser.f_name, reqObjUser.l_name, reqObjUser.email, reqObjUser.contact, DateToDBFormat(reqObjUser.dob), reqObjUser.gender, reqObjUser.eme_contact_name, reqObjUser.eme_contact_number, req.params.id];

                connection.query('UPDATE tbluser SET f_name = ?,  l_name = ?, email = ?, contact = ?, dob = ?, gender= ?, eme_contact_name = ?,eme_contact_number =? WHERE user_id=?', updateUserArr, (error, results, fields)=> {
                    if(error) {
                        console.log(error);
                        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                    } else {
                        res.status(200).send({"status": 200, "message": "User updated Succesfully!" , "success": true})
                    }
                })
            });

        } else {
            res.status(404).send({"status": 404 , "message": "User doesn't exist." , "success": false})
        }
       
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}

const deleteUser = async(req, res) => {
    try {
        let queryRes = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tbluser WHERE user_id = '${req.params.id}' AND is_active = 1) AS checkId`);
        queryRes = JSON.parse(JSON.stringify(queryRes))[0].checkId;
        
        if(queryRes === 1) {
            await runRawQuery(`UPDATE tbluser SET is_active = 0 WHERE user_id = ${req.params.id}`);
            res.status(200).send({"status": 200, "message": "User deleted Succesfully!", "success": true})
        } else {
            res.status(404).send({"status": 404 , "message": "User doesn't exist.", "success": false})
        }
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}




export { getUsers, updateUser, deleteUser };