import config from './env-config';

export default config[process.env.NODE_ENV];