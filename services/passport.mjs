import bcrypt from 'bcrypt';
import { get, runRawQuery } from '../db.mjs';
import { DateToDBFormat, validateReq } from '../helper.mjs';
import passport from 'passport';
import Strategy from 'passport-local';

passport.use(
    new Strategy({
            usernameField: 'email',
            passwordField: 'password',
            session: false
        },
        async (email, password, done) => {
            let getRes = await runRawQuery(`SELECT emp_id, f_name, l_name, email, password FROM tblemployees WHERE email= '${email}' and is_active='1'`);
            let userObj =JSON.parse(JSON.stringify(getRes))[0];
            console.log(userObj);
            if(userObj === undefined) {
                done(null, false)
            } else {
                bcrypt.compare(password, userObj.password, function(err, res) {
                    if(res) {
                        done(null, {
                            verified: true
                        });
                    } else {
                        done(null, false)
                    }
                });
            }
        }
    )
);

export { passport };
