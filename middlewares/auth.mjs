import { get, runRawQuery } from '../db.mjs';
import jwt from 'jsonwebtoken';

const serialize = async (req, res, next) => {
    let user = await runRawQuery(`SELECT emp_id, f_name, l_name, email FROM tblemployees WHERE 
    email = ${JSON.stringify(req.body.email)}`);
    if(user.length === 1) {
        req.user = {
            id: JSON.parse(JSON.stringify(user))[0].emp_id,
            f_name: JSON.parse(JSON.stringify(user))[0].f_name,
            l_name: JSON.parse(JSON.stringify(user))[0].l_name,
            email: JSON.parse(JSON.stringify(user))[0].email
        };
    }
    next();
}

const generateToken = (req, res, next) => {
    req.token = jwt.sign({
        id: req.user.id,
    }, 'server secret', {
        expiresIn : 7200
    });
    next();
}

const respond = (req, res) => {
    res.status(200).json({
        user: req.user,
        token: req.token
    });

}


export { serialize, generateToken, respond };