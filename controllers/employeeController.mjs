import { get , runRawQuery } from '../db.mjs';
import { DateToDBFormat, DateDDMMYY, validateReq } from '../helper.mjs';




const createEmployee = async (req, res) => {
    validateReq(req, res);

    try {
        let checkEmp = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tblemployees WHERE email = '${req.body.email}') AS checkEmpId`);
        checkEmp = JSON.parse(JSON.stringify(checkEmp))[0].checkEmpId;
    
        if(checkEmp === 0){
            const reqObj = req.body;

            get().getConnection((err, connection)=> {
                const createEmpArr = [[reqObj.f_name, reqObj.l_name, reqObj.email, reqObj.password, DateToDBFormat(reqObj.dob), reqObj.contact, reqObj.gender, reqObj.access_level, reqObj.eme_contact_name, reqObj.eme_contact_number, 1]];
                connection.query('INSERT INTO tblemployees (f_name, l_name, email, password, dob, contact, gender, access_level, eme_contact_name, eme_contact_number, is_active) VALUES (?)', createEmpArr, (error, results, fields) => {
                    if(error) {
                        console.log(error);
                        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
                    } else {
                        return res.status(200).send({"status": 200, "message": "Employee Created Succesfully.",  "success": true}); 
                    }
                })
            })

        } else {
            return res.status(422).send({"status": 422, "message": "Employee already exist" , "success": false});
        }
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
  
};

const getEmployee = async (req,res) => {
    try{

        let  {limit} = req.query
        if(limit  === undefined) {
            limit = 15;
        }
        let data = await runRawQuery(`SELECT emp_id, f_name, l_name, email, password, dob, gender , eme_contact_name, eme_contact_number contact FROM tblemployees WHERE is_active=1 LIMIT ${limit}`);

        for( let obj of data) {  
            obj.dob = DateDDMMYY(obj.dob)
        }
        let totalCount = await runRawQuery(`select count(*) as count from tblemployees where is_active=1`);
        totalCount = JSON.parse(JSON.stringify(totalCount))[0].count;

        let resObj = {
            "status": 200,
            "success": true,
            "limit": limit,
            "totalPages": Math.ceil(parseInt(totalCount)/limit),
            "message": "Employee data has been fetched successfully",
            "data": data

        }
        res.send(resObj);
       
    } catch(err) {
        console.log(err);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
}

const deleteEmployee = async(req, res, next) => {
    try {
        let empCheck = await runRawQuery(`SELECT EXISTS( SELECT 1 FROM tblemployees WHERE emp_id = '${req.params.id}' AND is_active = 1) AS checkId`);
        empCheck = JSON.parse(JSON.stringify(empCheck))[0].checkId;

        if(empCheck === 1) {
            await runRawQuery(` UPDATE tblemployees SET is_active = 0 WHERE emp_id = ${req.params.id}`);
            res.status(200).send({"status": 200, "message": "Employee deleted Succesfully!" , "success": true})
        } else {
            res.status(404).send({"status": 404 , "message": "Employee doesn't exist." , "success": false})
        }
    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
};

const updateEmployee = async(req, res, next) => {
    try {
        let empEmailCheck  = await runRawQuery(`SELECT EXISTS(SELECT 1 FROM tblemployees WHERE emp_id = '${req.params.id}' AND is_active = 1) AS checkId`);
        empEmailCheck = JSON.parse(JSON.stringify(empEmailCheck))[0].checkId
        
        if(empEmailCheck === 1){
            let reqData = req.body;
            const updateEmp = [reqData.f_name, reqData.l_name, DateToDBFormat(reqData.dob), reqData.contact, reqData.gender, reqData.eme_contact_name, reqData.eme_contact_number, req.params.id];

            get().getConnection((error, connection)=> {
                connection.query('UPDATE tblemployees SET f_name=?, l_name = ?, dob = ?, contact = ?, gender = ?, eme_contact_name = ?, eme_contact_number = ? WHERE emp_id = ?', updateEmp, (error, results, fields)=> {
                    if(error) {
                        console.log(e);
                        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false });
                    } else {
                        res.status(200).send({"status": 200, "message": "Employee updated Succesfully!" , "success": true});
                    }
                })
            })
        
        } else {
            res.status(404).send({"status": 404 , "message": "Employee doesn't exist." , "success": false})
        }

    } catch(e) {
        console.log(e);
        return res.status(500).send({"status": 500, "message": "Internal Server Error", "success": false});
    }
} 


export { createEmployee, getEmployee, deleteEmployee, updateEmployee}