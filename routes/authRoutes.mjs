import express from 'express';
import passport from 'passport';
import login from '../controllers/loginController.mjs';
import { serialize , generateToken , respond} from '../middlewares/auth.mjs';

let router = express.Router();

router.get('/auth',login);

router.post('/login', passport.authenticate(  
'local', {
    failureRedirect: '/auth',
    session: false
}), serialize, generateToken, respond );

export { router as authRoutes };