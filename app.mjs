import express from 'express';

import cookieSession from 'cookie-session';
import bodyParser from 'body-parser';
import config from './config/env';
import { authRoutes,appRoutes }  from './routes/index.mjs';
import { passport } from './services/passport.mjs';
import { connect } from './db';

import expressJwt from 'express-jwt';



const app = express();

app.use(bodyParser.json());

app.use(
  cookieSession({
    maxAge:  30 * 24 * 60 * 60 * 1000,
    keys: [config.cookieKey]
  })
);

const myLogger = (req,res, next) => {
    console.log('Routed to : ' + req.originalUrl);
    next();
};

app.use(myLogger);
app.use(expressJwt({credentialsRequired: true, secret: 'server secret', requestProperty: 'user'}).unless({path: ['/auth', '/login']}));

app.use(function(err, req, res, next) {
  if(err.name === 'UnauthorizedError') {
    res.status(err.status).send({message:err.message});
    return;
  }
  next();
});

app.use(passport.initialize());
app.use('/', authRoutes);
app.use('/api', appRoutes);

connect(config.env, (err) => {
  if(err){
    console.log('Unable to Connect to MYSQL');
    process.exit(1)
  } else {
    const PORT = process.env.PORT || config.port;
    app.listen(PORT, () => {
      console.log(`Listening on port`, PORT);
    });
  }
})
